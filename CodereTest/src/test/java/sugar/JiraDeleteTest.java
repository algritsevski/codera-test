package sugar;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

@Ignore
public class JiraDeleteTest {

	private static WebDriver driver;
	private final String baseUrl = "http://jira.entraction.com:8080";
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeClass
	public static void beforeClass() throws Exception {
		driver = new InternetExplorerDriver();
	}

	@Before
	public void setUp() throws Exception {
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	@Test
	public void testUntitled() throws Exception {
		driver.get(baseUrl
				+ "/login.jsp?os_destination=%2Fsecure%2FDashboard.jspa");

		driver.findElement(By.id("usernameinput")).clear();
		driver.findElement(By.id("usernameinput")).sendKeys("algr");
		driver.findElement(By.id("os_password")).clear();
		driver.findElement(By.id("os_password")).sendKeys("grigag1234.");
		driver.findElement(By.id("login")).click();

		driver.get(baseUrl
				+ "/secure/IssueNavigator.jspa?mode=hide&requestId=10876");

		for (WebElement issue; (issue = findElementQuetly(By
				.cssSelector("#issuetable td.issuekey a"))) != null;) {
			issue.click();

			driver.findElement(By.cssSelector("span.icon.drop-menu")).click();
			driver.findElement(By.id("delete-issue")).click();
			driver.findElement(By.id("deleteissue_submit")).click();
		}
	}

	private WebElement findElementQuetly(By by) {
		try {
			return driver.findElement(by);
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	private boolean isElementPresent(By by) {
		return findElementQuetly(by) != null;
	}
}
