package codere;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class UtilTest {

    @Test
    public void testGetResourceRoot() throws Exception {
        assertTrue(new File(Util.getResourceRoot(), "config.xml").exists());
    }

    @Test
    public void testGetTestResourceRoot() throws Exception {
        assertTrue(new File(Util.getTestResourceRoot(), "testData_test.xml").exists());
    }

    @Test
    public void testCreateUserId() throws Exception {
        assertTrue(Util.createUserId().length() > 5);
    }
}
