package codere;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import bo.LoginComponentBO;

import codere.CodereComponent;


public class CreateUsersTest {
		private CodereComponent component = CodereComponent.getInstance();
		private LoginComponentBO loginComponent = LoginComponentBO.getInstance();
		private WebDriver driver;
		
		String ID = "L211DS";
		int numberofusers =500;
		String password = "test1234";
		String opentiket = "office"; // "office" or "portal"
		
       @Before
	    public void setUp() throws Exception {
	        driver = component.getDriver();
	    }
       
       @Test
       public void testCreateUsers() throws Exception {
        
    	
        
        
    	 for (int i = 218; i <= numberofusers; i++) {
        component.navigate("portal?action=GoRegister");

       

        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(ID + i +"U");
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("vfy_password")).clear();
        driver.findElement(By.id("vfy_password")).sendKeys(password);
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(ID + i + "@igt.com");
        driver.findElement(By.id("vfy_email")).clear();
        driver.findElement(By.id("vfy_email")).sendKeys(ID + i + "@igt.com");

        new Select(driver.findElement(By.id("day"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("month"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("year"))).selectByVisibleText("1976");
       
        driver.findElement(By.id("agree")).click();
        driver.findElement(By.id("regContinue")).click();
        

        driver.findElement(By.id("response_1")).clear();
        driver.findElement(By.id("response_1")).sendKeys(ID + i);
        driver.findElement(By.id("fname")).clear();
        driver.findElement(By.id("fname")).sendKeys(ID + i + "First");
        driver.findElement(By.id("lname")).clear();
        driver.findElement(By.id("lname")).sendKeys(ID + i + "Last");
        driver.findElement(By.id("ssurname")).clear();
        driver.findElement(By.id("ssurname")).sendKeys(ID + i + "Surname");
        driver.findElement(By.xpath("(//input[@name='gender'])[2]")).click();
        driver.findElement(By.name("gender")).click();
        driver.findElement(By.id("addr_street_1")).clear();
        driver.findElement(By.id("addr_street_1")).sendKeys(ID + i + "Address");

        new Select(driver.findElement(By.id("addr_state_id"))).selectByVisibleText("Jalisco");

        driver.findElement(By.id("addr_city")).clear();
        driver.findElement(By.id("addr_city")).sendKeys(ID + i + "City");
        driver.findElement(By.id("telephone")).clear();
        driver.findElement(By.id("telephone")).sendKeys("1000000");
        
        driver.findElement(By.id("regFull")).click();
        assertTrue(component.isElementPresent(By.id("myAccount")));
       
        
      
        
    	 loginComponent.login();
    	 
    	component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

 		driver.findElement(By.name("Username")).clear();
 		driver.findElement(By.name("Username")).sendKeys(ID + i +"U");
 		driver.findElement(By.cssSelector("input.btn_def")).click();
 		
 		//Verification
 		new Select(driver.findElement(By.name("Status")))
 				.selectByVisibleText("Active");
 		new Select(driver.findElement(By.name("GoodEmail")))
 				.selectByVisibleText("Yes");
 		new Select(driver.findElement(By.name("GoodMobile")))
 				.selectByVisibleText("Yes");
 		new Select(driver.findElement(By.name("GoodAddr")))
 				.selectByVisibleText("Yes");
 		new Select(driver.findElement(By.name("BankCheck")))
 				.selectByVisibleText("Checked - OK");
 		driver.findElement(
 				By.cssSelector("th.buttons > table > tbody > tr > th.buttons > input.btn_def"))
 				.click();
 		new Select(driver.findElement(By.name("av_status"))).selectByVisibleText("Verified");
 		new Select(driver.findElement(By.name("av_reason_code"))).selectByVisibleText("Verified by Credit Safe");
 		driver.findElement(By.xpath("//input[@value='Update']")).click();
 		
 		driver.findElement(By.xpath("//input[@value='View Customer Flags']")).click();
 		driver.findElement(By.name("F_BIR_DELAY_FACTOR")).clear();
 		driver.findElement(By.name("F_BIR_DELAY_FACTOR")).sendKeys("1");
 		new Select(driver.findElement(By.name("F_VERIFIED_EMAIL"))).selectByVisibleText("Y");
 		driver.findElement(By.cssSelector("input.btn_def")).click();
 		 		   	
 			
 		//Manual Adjustment	
 			
	 	new Select(driver.findElement(By.id("SubType")))
				.selectByVisibleText("Payment");
		driver.findElement(By.name("Description")).clear();
		driver.findElement(By.name("Description")).sendKeys("TestDescrition");
		driver.findElement(By.name("Amount")).clear();
		driver.findElement(By.name("Amount")).sendKeys("100000");
		driver.findElement(By.name("MadjOperNotes")).clear();
		driver.findElement(By.name("MadjOperNotes")).sendKeys("Operator Notes");
		driver.findElement(By.cssSelector("td.buttons > input.btn_def"))
				.click();
		driver.switchTo().alert().accept();
	
		component.navigateBO("/admin?action=ADMIN::ADJ::GoAdjQry");
		driver.findElement(By.name("SR_acct_no")).clear();
		driver.findElement(By.name("SR_acct_no")).sendKeys(ID + i +"U");
		driver.findElement(By.xpath("//input[@value='Find Adjustments']"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/table/tbody/tr[3]/td[11]/input"))
				.click();
		driver.findElement(By.xpath("//input[@value='Authorise Adjustments']"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/table/tbody/tr[3]/td[12]/input"))
				.click();
		driver.findElement(By.xpath("//input[@value='Post Adjustments']"))
				.click();	
		
		//Open Ticket
		
		if (opentiket == "portal" ) {
		
		component.navigate("portal");
             
        driver.findElement(By.id("LoginUsername")).sendKeys(ID + i +"U" );
        driver.findElement(By.id("LoginPassword")).clear();
        driver.findElement(By.id("LoginPassword")).sendKeys(password);
        driver.findElement(
        By.cssSelector("#login > div.loginFields > #gpFmLogin > fieldset > div.submitBox > input[name=\"login\"]"))
                    .click();
       
        
        component.navigate("portal?action=GoAccount");
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Mi Cuenta[\\s\\S]*$"));
        
		component.isElementPresent(By.id("amount"));
		
		if (component.isElementPresent(By.linkText("Finalizar Ticket"))) {
			driver.findElement(By.linkText("Finalizar Ticket")).click();
			driver.findElement(By.id("confirmAction")).click();
			
		}
		
		driver.findElement(By.id("amount")).clear();
		driver.findElement(By.id("amount")).sendKeys("50000");
		driver.findElement(By.id("openTicket")).click();
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Ticket Casino Activo[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*MXN 50000\\.00[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*MXN 50000\\.00[\\s\\S]*$"));
        
		}
		
		if (opentiket == "office"){
		
		component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

 		driver.findElement(By.name("Username")).clear();
 		driver.findElement(By.name("Username")).sendKeys(ID + i +"U");
 		driver.findElement(By.cssSelector("input.btn_def")).click();
 		driver.findElement(By.xpath("//input[@value='Ticket Management']")).click();
 		
 		if (component.isElementPresent(By.xpath("//input[@value='Settle Ticket']"))) {
			  driver.findElement(By.xpath("//input[@value='Settle Ticket']")).click();
		}
		
		driver.findElement(By.name("amount")).clear();
	    driver.findElement(By.name("amount")).sendKeys("50000");
	    driver.findElement(By.cssSelector("input.btn_def")).click();
	    
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Ticket created successful[\\s\\S]*$"));
	    
    	 }
		
 		}
 		
       }
      }
 		
 

       
       
