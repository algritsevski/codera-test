package gaming;

import codere.CodereComponent;
import domain.Config;
import domain.GlobalConfig;
import domain.User;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertTrue;

public class CodereCreateExtendedUser {

    private CodereComponent component = CodereComponent.getInstance();

    private GlobalConfig globalConfig = GlobalConfig.getInstance();

    private Config config = Config.getInstance();

    @Test
    public void testRegForm() throws Exception {
        User u = new User(String.valueOf(Integer.parseInt(getConfig().getId()) + 1));
        u.setDepartment(getConfig().getDepartment());
        u.setPassword(getConfig().getPassword());

        UserCreator createUser = createUser();
        createUser.add(u);

        System.out.println(u);
        assertTrue(component.isElementPresent(By.id("myAccount")));

        config.setUser(u);
    }

    UserCreator createUser() {
        return new CreateExtendedUser();
    }

    private User getConfig() {
        return config.getUser();
    }
}
