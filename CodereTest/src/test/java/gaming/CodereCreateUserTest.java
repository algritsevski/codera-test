package gaming;

import codere.CodereComponent;
import codere.Util;
import domain.FileConfig;
import domain.GlobalConfig;
import domain.User;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class CodereCreateUserTest {

    private CodereComponent component = CodereComponent.getInstance();

    private GlobalConfig globalConfig = GlobalConfig.getInstance();

    private FileConfig config;

    @SuppressWarnings("deprecation")
    @Before
    public void before() throws Exception {
        config = new FileConfig(new File(Util.getTestResourceRoot(), "testData_" + globalConfig.getEnvironment() + ".xml"));
    }

    @Test
    public void testRegForm() throws Exception {
        final String userId = Util.createUserId();
        User u = new User(userId) {
            @Override
            public String getName() {
                return userId;
            }
        };
        u.setDepartment(config.getUser().getDepartment());
        u.setPassword(config.getUser().getPassword());

        new CreateUser().add(u);

        System.out.println(u);

        assertTrue(component.isElementPresent(By.id("myAccount")));
    }
}
