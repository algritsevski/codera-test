package common;

import bo.ManualAdjastmentBO;
import bo.TiketManagamentBO;
import bo.VerifyUserBO;
import gaming.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({

        CodereCreateUserTest.class,
        CodereCreateExtendedUser.class,
        VerifyUserBO.class,
        ManualAdjastmentBO.class,
        CodereNavigateMyAccount.class,
        CodereDepositVerification.class,
        CodereOpenTicket.class,
        CodereLaunchingGame.class,
        TiketManagamentBO.class
})
public class CodereTestSuite {
}
