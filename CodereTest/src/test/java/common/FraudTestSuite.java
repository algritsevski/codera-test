package common;

import bo.VerifyUserBO;
import gaming.CodereDepositLimitsVerification;
import gaming.CodereSelfExcludeVerification;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	
        VerifyUserBO.class,
        CodereSelfExcludeVerification.class,
        CodereDepositLimitsVerification.class
})
public class FraudTestSuite {
}
