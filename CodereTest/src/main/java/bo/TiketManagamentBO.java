package bo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import codere.CodereComponent;
import domain.User;


public class TiketManagamentBO {
	
	private CodereComponent component = CodereComponent.getInstance();
	private LoginComponentBO loginComponent = LoginComponentBO.getInstance();

	private User user;
	private WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
		user = component.findUser();

		loginComponent.login();
		
		component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

		driver.findElement(By.name("Username")).clear();
		driver.findElement(By.name("Username")).sendKeys(user.getName());
		driver.findElement(By.cssSelector("input.btn_def")).click();
		driver.findElement(By.xpath("//input[@value='Ticket Management']")).click();
		
	}

	@Test
	public void testOpenTiketBO() throws Exception {
		
		if (component.isElementPresent(By.xpath("//input[@value='Settle Ticket']"))) {
			  driver.findElement(By.xpath("//input[@value='Settle Ticket']")).click();
		}
		
		driver.findElement(By.name("amount")).clear();
	    driver.findElement(By.name("amount")).sendKeys("10000");
	    driver.findElement(By.cssSelector("input.btn_def")).click();
	    //must be change to correct message
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Ticket created successful[\\s\\S]*$"));
	
		
		driver.findElement(By.name("amount")).clear();
	    driver.findElement(By.name("amount")).sendKeys("5000");
	    driver.findElement(By.cssSelector("input.btn_def")).click();
	    assertEquals("Ticket topup successful", driver.findElement(By.cssSelector("h3.infoyes")).getText());
	    
	
				
	    driver.findElement(By.xpath("//input[@value='Settle Ticket']")).click();
	    assertEquals("Ticket settlement successful: amount 15000.00", driver.findElement(By.cssSelector("h3.infoyes")).getText());
	}
	
	
}
	


