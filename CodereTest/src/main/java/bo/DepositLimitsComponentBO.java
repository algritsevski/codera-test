package bo;

import codere.CodereComponent;
import domain.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static junit.framework.Assert.assertEquals;

public class DepositLimitsComponentBO {

    private static final DepositLimitsComponentBO INSTANCE = new DepositLimitsComponentBO();

    private LoginComponentBO loginComponent = LoginComponentBO.getInstance();
    private CodereComponent component = CodereComponent.getInstance();

    private WebDriver driver = component.getDriver();
    private String DepositIn;
    private User user;

    Date date = new Date();
    Calendar calendar = GregorianCalendar.getInstance();
    String inputdate;

    private DepositLimitsComponentBO() {
    }

    public static DepositLimitsComponentBO getInstance() {
        return INSTANCE;
    }

    public void depositlimits() throws Exception {
        if (isDepositIn()) {
            return;
        }
        driver = component.getDriver();
        user = component.findUser();

        loginComponent.login();

        calendar.setTime(date);
        inputdate = "" + calendar.get(Calendar.DAY_OF_MONTH);

        component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");


        driver.findElement(By.name("Username")).clear();
        driver.findElement(By.name("Username")).sendKeys(user.getName());
        driver.findElement(By.cssSelector("input.btn_def")).click();

        driver.findElement(By.name("daydep_limit_text")).click();
        driver.findElement(By.name("daydep_limit_text")).clear();
        driver.findElement(By.name("daydep_limit_text")).sendKeys("");
        driver.findElement(By.xpath("//input[@value='Update Daily Limit']")).click();
        extracted();

        driver.findElement(By.name("weekdep_limit_text")).click();
        driver.findElement(By.name("weekdep_limit_text")).clear();
        driver.findElement(By.name("weekdep_limit_text")).sendKeys("");
        driver.findElement(By.xpath("//input[@value='Update Weekly Limit']")).click();
        extracted();

        driver.findElement(By.name("monthdep_limit_text")).click();
        driver.findElement(By.name("monthdep_limit_text")).clear();
        driver.findElement(By.name("monthdep_limit_text")).sendKeys("");
        driver.findElement(By.xpath("//input[@value='Update Monthly Limit']")).click();
        extracted();


        new Select(driver.findElement(By.name("GoodEmail"))).selectByVisibleText("Yes");
        new Select(driver.findElement(By.name("GoodMobile"))).selectByVisibleText("Yes");
        new Select(driver.findElement(By.name("GoodAddr"))).selectByVisibleText("Yes");
        new Select(driver.findElement(By.name("Status"))).selectByVisibleText("Active");
        driver.findElement(By.cssSelector("th.buttons > table > tbody > tr > th.buttons > input.btn_def")).click();
    }

	@SuppressWarnings("deprecation")
	private void extracted() {
		assertEquals("Your deposit limit has been successfully updated.", driver.findElement(By.cssSelector("h3.infoyes")).getText());
	}

    public boolean isDepositIn() {
        return DepositIn != null;
    }

    public String getDepositResult() {
        return DepositIn;
    }
}
