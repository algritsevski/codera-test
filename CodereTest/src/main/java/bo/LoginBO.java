package bo;

import codere.CodereComponent;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LoginBO {

	private CodereComponent component = CodereComponent.getInstance();
	private LoginComponentBO loginComponent = LoginComponentBO.getInstance();

	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
	}

	@Test
	public void testLoginBO() throws Exception {
		loginComponent.login();

		assertTrue(loginComponent.isLoggedIn());
		assertEquals("OpenBet Office", loginComponent.getLoginResult());
	}
}
