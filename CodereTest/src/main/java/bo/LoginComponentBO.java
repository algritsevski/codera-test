package bo;

import codere.CodereComponent;
import domain.Config;
import domain.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginComponentBO {

    private static final LoginComponentBO INSTANCE = new LoginComponentBO();

    private Config config = Config.getInstance();

    private CodereComponent component = CodereComponent.getInstance();
    private WebDriver driver = component.getDriver();

    private User user;
    private String loggedIn;

    private LoginComponentBO() {
    }

    public static LoginComponentBO getInstance() {
        return INSTANCE;
    }

    public void login() {
        if (isLoggedIn()) {
            return;
        }

        user = getBackOfficeUser();
        component.navigateBO("office");

        driver.findElement(By.id("loginUsername")).clear();
        driver.findElement(By.id("loginUsername")).sendKeys(user.getId());

        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(user.getPassword());

        driver.findElement(By.name("login")).click();

        loggedIn = driver.getTitle();
    }

    public User getBackOfficeUser() {
        User u = new User(config.getProperty("usernameBO"));

        u.setPassword(config.getProperty("passwordBO"));

        return u;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public String getLoginResult() {
        return loggedIn;
    }
}
