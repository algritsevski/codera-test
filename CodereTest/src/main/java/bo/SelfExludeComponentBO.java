package bo;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;
import domain.User;

public class SelfExludeComponentBO {

	private static final SelfExludeComponentBO INSTANCE = new SelfExludeComponentBO();
	private LoginComponentBO loginComponent = LoginComponentBO.getInstance();
	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver = component.getDriver();
	private String ExcludedIn;
	private User user;
	    
	Date date = new Date();
	Calendar calendar = GregorianCalendar.getInstance();
	String inputdate;
	
	private SelfExludeComponentBO() {
	}

	public static SelfExludeComponentBO getInstance() {
		return INSTANCE;
	}

	public void selfexlude() throws Exception {
		if (isExcludedIn()) {
			return;
		}
		driver = component.getDriver();
		user = component.findUser();

		loginComponent.login();
		
		calendar.setTime(date);
		inputdate = "" + calendar.get(Calendar.DAY_OF_MONTH);
		
		component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

		
		driver.findElement(By.name("Username")).clear();
		driver.findElement(By.name("Username")).sendKeys(user.getName());
		driver.findElement(By.cssSelector("input.btn_def")).click();
		
		
		driver.findElement(By.id("calendarImg0")).click();
		driver.findElement(By.linkText(inputdate)).click();
		driver.findElement(By.xpath("//input[@value='Update Self Exclusion']")).click();
		driver.switchTo().alert().accept();
		
        component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

		
		driver.findElement(By.name("Username")).clear();
		driver.findElement(By.name("Username")).sendKeys(user.getName());
		driver.findElement(By.cssSelector("input.btn_def")).click();
		
		driver.findElement(By.cssSelector("input.btn_def")).click();
		
		
		
		new Select(driver.findElement(By.name("GoodEmail"))).selectByVisibleText("Yes");
		new Select(driver.findElement(By.name("GoodMobile"))).selectByVisibleText("Yes");
		new Select(driver.findElement(By.name("GoodAddr"))).selectByVisibleText("Yes");
        new Select(driver.findElement(By.name("Status"))).selectByVisibleText("Active");
		driver.findElement(By.cssSelector("th.buttons > table > tbody > tr > th.buttons > input.btn_def")).click();
	}
	
	public boolean isExcludedIn() {
		return ExcludedIn != null;
	}

	public String getSelfExludeResult() {
		return ExcludedIn;
	}
}
