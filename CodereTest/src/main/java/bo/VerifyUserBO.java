package bo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;
import domain.User;

public class VerifyUserBO {

	private CodereComponent component = CodereComponent.getInstance();
	private LoginComponentBO loginComponent = LoginComponentBO.getInstance();

	private User user;
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
		user = component.findUser();

		loginComponent.login();
	}

	@Test
	public void testVerifyUserBO() throws Exception {
		component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

		driver.findElement(By.name("Username")).clear();
		driver.findElement(By.name("Username")).sendKeys(user.getName());
		driver.findElement(By.cssSelector("input.btn_def")).click();
		
		new Select(driver.findElement(By.name("Status")))
				.selectByVisibleText("Active");
		new Select(driver.findElement(By.name("GoodEmail")))
				.selectByVisibleText("Yes");
		new Select(driver.findElement(By.name("GoodMobile")))
				.selectByVisibleText("Yes");
		new Select(driver.findElement(By.name("GoodAddr")))
				.selectByVisibleText("Yes");
		new Select(driver.findElement(By.name("BankCheck")))
				.selectByVisibleText("Checked - OK");
		driver.findElement(
				By.cssSelector("th.buttons > table > tbody > tr > th.buttons > input.btn_def"))
				.click();
		new Select(driver.findElement(By.name("av_status"))).selectByVisibleText("Verified");
		new Select(driver.findElement(By.name("av_reason_code"))).selectByVisibleText("Verified by Credit Safe");
		driver.findElement(By.xpath("//input[@value='Update']")).click();
		
		driver.findElement(By.xpath("//input[@value='View Customer Flags']")).click();
		driver.findElement(By.name("F_BIR_DELAY_FACTOR")).clear();
		driver.findElement(By.name("F_BIR_DELAY_FACTOR")).sendKeys("1");
		new Select(driver.findElement(By.name("F_VERIFIED_EMAIL"))).selectByVisibleText("Y");
		driver.findElement(By.cssSelector("input.btn_def")).click();
		
		driver.findElement(
				By.xpath("//input[@value='Amend Status Flags']"))
				.click();

		for (WebElement issue; (issue = component.findElementQuetly(By
				.linkText("[clear]"))) != null;) {
			issue.click();
			driver.switchTo().alert().accept();
		}
	}
}
