package bo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;
import domain.User;

public class ManualAdjastmentBO {

	private CodereComponent component = CodereComponent.getInstance();
	private LoginComponentBO loginComponent = LoginComponentBO.getInstance();

	private User user;
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
		user = component.findUser();

		loginComponent.login();
	}

	@Test
	public void testManualAdjastmentBO() throws Exception {
		component.navigateBO("admin?action=ADMIN::CUST::GoCustQuery");

		driver.findElement(By.name("Username")).clear();
		driver.findElement(By.name("Username")).sendKeys(user.getName());
		driver.findElement(By.cssSelector("input.btn_def")).click();

		new Select(driver.findElement(By.id("SubType")))
				.selectByVisibleText("Payment");
		driver.findElement(By.name("Description")).clear();
		driver.findElement(By.name("Description")).sendKeys("TestDescrition");
		driver.findElement(By.name("Amount")).clear();
		driver.findElement(By.name("Amount")).sendKeys("100000");
		driver.findElement(By.name("MadjOperNotes")).clear();
		driver.findElement(By.name("MadjOperNotes")).sendKeys("Operator Notes");
		driver.findElement(By.cssSelector("td.buttons > input.btn_def"))
				.click();
		driver.switchTo().alert().accept();

		component.navigateBO("/admin?action=ADMIN::ADJ::GoAdjQry");
		driver.findElement(By.name("SR_acct_no")).clear();
		driver.findElement(By.name("SR_acct_no")).sendKeys(user.getName());
		driver.findElement(By.xpath("//input[@value='Find Adjustments']"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/table/tbody/tr[3]/td[11]/input"))
				.click();
		driver.findElement(By.xpath("//input[@value='Authorise Adjustments']"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/table/tbody/tr[3]/td[12]/input"))
				.click();
		driver.findElement(By.xpath("//input[@value='Post Adjustments']"))
				.click();

	}
}
