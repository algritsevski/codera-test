package codere;

import domain.Config;
import domain.User;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class CodereComponent {

    private static final CodereComponent INSTANCE = new CodereComponent();

    private Config config = Config.getInstance();

    private WebDriver driver;

    private CodereComponent() {
    	System.setProperty("webdriver.gecko.driver", "/seliniumDrivers");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
    }

    public static CodereComponent getInstance() {
        return INSTANCE;
    }

    @SuppressWarnings("deprecation")
    public User findUser() throws Exception {
        return config.getUser();
    }

    public void navigate(String s) {
        if (!s.startsWith("/")) {
            navigate("/" + s);
            return;
        }

        driver.get(config.getPortalUrl().toString() + s);
    }
    
    public void navigateBO(String s) {
        if (!s.startsWith("/")) {
            navigateBO("/" + s);
            return;
        }

        driver.get(config.getBOUrl().toString() + s);
    }
    public void waitElement(By by) throws InterruptedException {
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (isElementPresent(by)) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement findElementQuetly(By by) {
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

    public boolean isElementPresent(By by) {
        return findElementQuetly(by) != null;
    }
}
