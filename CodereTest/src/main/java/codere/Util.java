package codere;

import java.io.File;
import java.security.SecureRandom;
import java.util.Random;

public class Util {

    private static final Random RANDOM = new SecureRandom();

    public static File getResourceRoot() {
        return new File(getProjectRoot(), "src/main/resources/codere");
    }

    public static File getTestResourceRoot() {
        return new File(getProjectRoot(), "src/test/resources/codere");
    }

    public static File getProjectRoot() {
        return new File(".");
    }

    public static String createUserId() {
        return new StringBuilder(8)
                .append((char) getRandomNextInt(65, 90))
                .append(getRandomNextString(6))
                .append((char) getRandomNextInt(97, 122))
                .toString();
    }

    static int getRandomNextInt(int beg, int end) {
        int code = 0;
        while (code < beg) {
            code = RANDOM.nextInt(end);
        }
        return code;
    }

    static String getRandomNextString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(RANDOM.nextInt(9));
        }
        return sb.toString();
    }
}
