package codere;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigComponent {

    Properties p = new Properties();

    public ConfigComponent(File resource) throws IOException {
        p.loadFromXML(new FileInputStream(resource));
    }

    public Properties getProperties() {
        return p;
    }

    public String getProperty(String key, String defaultValue) {
        return p.getProperty(key, defaultValue);
    }
}
