package domain;

import codere.ConfigComponent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class FileConfig {

    private final ConfigComponent configComponent;

    private final File resource;

    User user;

    public FileConfig(File r) throws IOException {
        resource = r;

        configComponent = new ConfigComponent(resource);

        init();
    }

    void init() {
        user = new User(configComponent.getProperty("usernumber", null));
        user.setDepartment(configComponent.getProperty("department", null));
        user.setPassword(configComponent.getProperty("password", null));
    }

    public User getUser() {
        return user;
    }

    public void write(User user) throws IOException {
        Properties properties = configComponent.getProperties();
        properties.setProperty("usernumber", user.getId());

        OutputStream os = new FileOutputStream(resource);
        try {
            properties.storeToXML(os, null);
        } finally {
            os.flush();
            os.close();
        }
    }

    String getProperty(String key, String defaultValue) {
        return configComponent.getProperty(key, defaultValue);
    }
}
