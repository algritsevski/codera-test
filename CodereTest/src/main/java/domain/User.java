package domain;

public class User {

    String id;

    String department;

	String password;

   
    public User(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getName() {
        return department + getId();
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" + getName() + '}';
    }
}
