package domain;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Config {

    private static final Config INSTANCE = new Config();

    private final GlobalConfig globalConfig = GlobalConfig.getInstance();
    private final FileConfig config;

    private Config() {
        try {
            config = new FileConfig(globalConfig.getEnvironmentConfigFile());
        } catch (IOException e) {
            throw new RuntimeException("Failed globalConfig loading from " + globalConfig.getEnvironmentConfigFile(), e);
        }
    }

    public static Config getInstance() {
        return INSTANCE;
    }

    public User getUser() {
        return config.getUser();
    }

    public void setUser(User u) throws Exception {
        config.write(u);
        config.init();
    }

    public String getProperty(String key) {
        String value = config.getProperty(key, null);
        if (value == null) {
            throw new RuntimeException("Not found property=[" + key + "] for globalConfig=[" + globalConfig.getEnvironmentConfigFile() + "]");
        }
        return value;
    }

    public URL getPortalUrl() {
        try {
            return new URL(config.getProperty("url", null));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failed loading portal URL for globalConfig=[" + globalConfig.getEnvironmentConfigFile() + "]", e);
        }
    }
    
    public URL getBOUrl() {
        try {
            return new URL(config.getProperty("urlBO", null));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failed loading BO URL for globalConfig=[" + globalConfig.getEnvironmentConfigFile() + "]", e);
        }
    }
    
}
