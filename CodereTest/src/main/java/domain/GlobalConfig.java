package domain;

import codere.ConfigComponent;
import codere.Util;

import java.io.File;
import java.io.IOException;

public class GlobalConfig {

    private static final GlobalConfig INSTANCE = new GlobalConfig();

    private ConfigComponent configComponent;

    private GlobalConfig() {
        try {
            configComponent = new ConfigComponent(new File(Util.getResourceRoot(), "config.xml"));
        } catch (IOException e) {
            throw new RuntimeException("Failed config loading", e);
        }
    }

    public static GlobalConfig getInstance() {
        return INSTANCE;
    }

    public String getEnvironment() {
        return configComponent.getProperty("environment", "");
    }

    public File getEnvironmentConfigFile() {
        return new File(Util.getTestResourceRoot(), "testData_" + getEnvironment() + ".xml");
    }
}
