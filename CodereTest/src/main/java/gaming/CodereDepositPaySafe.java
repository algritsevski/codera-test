package gaming;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;
import domain.User;
public class CodereDepositPaySafe {
	
	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	private static User user;
	
	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
	}

	@Test
	public void testCodereDepositVerivication() throws Exception {
		component.navigate("portal");
		user = component.findUser();
		
		driver.findElement(By.cssSelector("a.depositBtn")).click();

		
		driver.findElement(By.cssSelector("a.paySafeCard")).click();

		if (component
				.isElementPresent(By
						.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"amount\"]")))
			;
		else
			fail("PaySafe Card");
		
		 driver.findElement(By.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"amount\"]")).clear();
		    driver.findElement(By.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"amount\"]")).sendKeys("6364070100204055");
		    driver.findElement(By.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"password\"]")).clear();
		    driver.findElement(By.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"password\"]")).sendKeys("101504055");
		    driver.findElement(By.linkText("Depositar")).click();
		    
		    if (component.isElementPresent(By.cssSelector("#loginErrorBox > p")))
			      
		    {  assertEquals("Contraseña incorrecta.\nEl pago solicitado supera la cantidad máxima permitida, por favor introduce una cantidad dentro de los límites de pago", driver.findElement(By.cssSelector("#loginErrorBox > p")).getText());}
		    
		
		
	}
}

