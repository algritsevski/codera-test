package gaming;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;
import domain.User;

public class CreateUser implements UserCreator {

    private CodereComponent component = CodereComponent.getInstance();

    public void add(User user) {
        WebDriver driver = component.getDriver();

        component.navigate("portal");

        String userId = user.getName();

        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(userId);
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys(user.getPassword());
        driver.findElement(By.id("vfy_password")).clear();
        driver.findElement(By.id("vfy_password")).sendKeys(user.getPassword());
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(userId + "@igt.com");
        driver.findElement(By.id("vfy_email")).clear();
        driver.findElement(By.id("vfy_email")).sendKeys(userId + "@igt.com");

        new Select(driver.findElement(By.id("day"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("month"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("year"))).selectByVisibleText("1976");

        driver.findElement(By.id("agree")).click();
        driver.findElement(By.id("regFun")).click();
        driver.findElement(By.id("regFun")).click();
    }
}
