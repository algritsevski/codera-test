package gaming;



import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;

public class CodereLaunchingGame {

	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();

	}

	@Test
	// Codere ST1.11 - Launching a play-money game from the Casino/Bingo page:
	// Codere ST1.10 - Launching a real-money game from the Casino/Bingo page:
	public void testCodereLaunchingGame() throws Exception {

		component.navigate("portal");

		if (component.isElementPresent(By.linkText("Bingo"))) {

			driver.findElement(By.linkText("Bingo")).click();

			component.waitElement(By.linkText("Juega ya"));

			// real-money
			driver.findElement(By.id("cDiv")).click();
			driver.findElement(By.linkText("Juega ya")).click();

			driver.findElement(By.cssSelector("div.gameRating")).click();
			driver.findElement(By.linkText("5")).click();
			component.navigate("portal");
			//driver.findElement(By.linkText("×")).click();

			// play-money
			driver.findElement(By.linkText("Bingo")).click();
			driver.findElement(By.linkText("Pruébalo")).click();
			
			driver.findElement(By.cssSelector("div.gameRating")).click();
			driver.findElement(By.linkText("5")).click();
			component.navigate("portal");
			//driver.findElement(By.linkText("×")).click();
		}

	}
}
