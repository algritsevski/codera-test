package gaming;

import codere.CodereComponent;
import domain.User;
import org.junit.rules.ExternalResource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CodereLogin extends ExternalResource {

    private static User user;

    private CodereComponent component = CodereComponent.getInstance();
    private WebDriver driver;

    @Override
    protected void before() throws Throwable {
        if (user != null) {
            return;
        }

        driver = component.getDriver();
        user = component.findUser();

        login();

        if (!component.isElementPresent(By.id("myAccount"))) {
            throw new RuntimeException("Login failed for user=" + user);
        }
    }

    public void login() throws Exception {
        component.navigate("portal");
        user = component.findUser();

        if (component.isElementPresent(By.id("LoginUsername"))) {
            driver.findElement(By.id("LoginUsername")).clear();
            driver.findElement(By.id("LoginUsername")).sendKeys(user.getName());
            driver.findElement(By.id("LoginPassword")).clear();
            driver.findElement(By.id("LoginPassword")).sendKeys(user.getPassword());
            driver.findElement(
                    By.cssSelector("#login > div.loginFields > #gpFmLogin > fieldset > div.submitBox > input[name=\"login\"]"))
                    .click();
        }
    }

    public void logout() {
        user = null;
    }
}
