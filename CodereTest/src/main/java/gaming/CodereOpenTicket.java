package gaming;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;

public class CodereOpenTicket {

	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
		component.navigate("portal?action=GoAccount");
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Mi Cuenta[\\s\\S]*$"));
		
	}

	@Test
	public void testCodereOpenTicket() throws Exception {
		
		component.isElementPresent(By.id("amount"));
		
		if (component.isElementPresent(By.linkText("Cerrar Ticket"))) {
			driver.findElement(By.linkText("Cerrar Ticket")).click();
			driver.findElement(By.id("confirmAction")).click();
			
		}
		
		driver.findElement(By.id("topupTicketAmount")).clear();
	    driver.findElement(By.id("topupTicketAmount")).sendKeys("10000");
	    driver.findElement(By.id("openTicket")).click();
		assertEquals("90000.00 Pesos", driver.findElement(By.cssSelector("td.bold")).getText());
		//assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*MXN 10000\\.00[\\s\\S]*$"));
	}
	
	@Test
	public void testCodereFinalizeTicket() throws Exception {
		driver.findElement(By.linkText("Cerrar Ticket")).click();
		driver.findElement(By.id("confirmAction")).click();
		assertEquals("100000.00 Pesos", driver.findElement(By.cssSelector("td.bold")).getText());
		//assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*0\\.00 MXN[\\s\\S]*$"));
	}	
	
	@Test
	public void testCodereOpenAddmoreTicket() throws Exception {
		driver.findElement(By.id("topupTicketAmount")).clear();
	    driver.findElement(By.id("topupTicketAmount")).sendKeys("10000");
	    driver.findElement(By.id("openTicket")).click();
	    driver.findElement(By.id("topupTicketAmount")).clear();
	    driver.findElement(By.id("topupTicketAmount")).sendKeys("5000");
	    driver.findElement(By.linkText("Añadir")).click();
	    assertEquals("85000.00 Pesos", driver.findElement(By.cssSelector("td.bold")).getText());
	    //assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*MXN 15000\\.00[\\s\\S]*$"));
	}
}
