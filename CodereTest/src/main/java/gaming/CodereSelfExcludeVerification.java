package gaming;


import static org.junit.Assert.assertTrue;

import bo.SelfExludeComponentBO;
import org.junit.Before;
import org.junit.ClassRule;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;

public class CodereSelfExcludeVerification {

	String duration;
	
	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private SelfExludeComponentBO  exludeComponent = SelfExludeComponentBO .getInstance();
	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
	}


	@Test
	public void testSelfExcludeVerification() throws Exception {
		
	for (int count = 0; count < 6; count ++) {
	
	if ( count == 0 ) {duration= "6 Meses";}			
	if ( count == 1) {duration= "1 Año";}
	if ( count == 2) {duration= "2 Años";}
	if ( count == 3) {duration= "3 Años";}
	if ( count == 4) {duration= "4 Años";}	
	if ( count == 5) {duration= "5 Años";}	
	
	
		component.navigate("portal?action=GoResponsible");

		new Select(driver.findElement(By.name("excl_period")))
				.selectByVisibleText(duration);

		driver.findElement(By.name("submit1")).click();
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Esto significa que no podrá jugar a ningún juego\\. También se le desconectará de la página web, y no será capaz de acceder hasta que el período de autoexclusión haya terminado\\.[\\s\\S]*$"));
		driver.findElement(By.id("confirmAction")).click();

		// logout
		assertTrue(component.isElementPresent(By.cssSelector("#loginErrorBox")));
		login.logout();
		
		login.login();
		assertTrue(component.isElementPresent(By.cssSelector("#loginErrorBox")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Tu cuenta ha sido suspendida debido a la solicitud de autoexclusión\\.[\\s\\S]*$"));
		login.logout();
		
		exludeComponent.selfexlude();
		login.login();
		assertTrue(component.isElementPresent(By.id("myAccount")));
		assertTrue(component.isElementPresent(By.cssSelector("a.depositBtn")));
		login.logout();
		
		}
	
	for (int count = 0; count < 6; count ++) {
		
		if ( count == 0 ) {duration= "6 Meses";}			
		if ( count == 1) {duration= "1 Año";}
		if ( count == 2) {duration= "2 Años";}
		if ( count == 3) {duration= "3 Años";}
		if ( count == 4) {duration= "4 Años";}	
		if ( count == 5) {duration= "5 Años";}	
		
		
			component.navigate("portal?action=GoResponsible");

			new Select(driver.findElement(By.name("excl_period")))
					.selectByVisibleText(duration);

			driver.findElement(By.name("submit1")).click();
			driver.findElement(By.linkText("Cancelar")).click();


			login.logout();
			
			login.login();
			
			assertTrue(component.isElementPresent(By.id("myAccount")));
			assertTrue(component.isElementPresent(By.cssSelector("a.depositBtn")));
			login.logout();
			
			}
	
	for (int count = 0; count < 6; count ++) {
		
		if ( count == 0 ) {duration= "6 Meses";}			
		if ( count == 1) {duration= "1 Año";}
		if ( count == 2) {duration= "2 Años";}
		if ( count == 3) {duration= "3 Años";}
		if ( count == 4) {duration= "4 Años";}	
		if ( count == 5) {duration= "5 Años";}	
		
		
			component.navigate("portal?action=GoResponsible");

			new Select(driver.findElement(By.name("excl_period")))
					.selectByVisibleText(duration);

			driver.findElement(By.name("submit1")).click();
			driver.findElement(By.id("closeLoginPopup")).click();


			login.logout();
			
			login.login();
			
			assertTrue(component.isElementPresent(By.id("myAccount")));
			assertTrue(component.isElementPresent(By.cssSelector("a.depositBtn")));
			login.logout();
			
			}
	
	}
}
