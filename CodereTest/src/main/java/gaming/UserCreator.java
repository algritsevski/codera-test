package gaming;

import domain.User;

public interface UserCreator {

    void add(User user);
}
