package gaming;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;
import domain.User;
public class CodereDepositNetteler {
	
	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	private static User user;
	
	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
	}

	@Test
	public void testCodereDepositVerivication() throws Exception {
		component.navigate("portal");
		user = component.findUser();
		
		driver.findElement(By.cssSelector("a.depositBtn")).click();

		
		driver.findElement(By.cssSelector("a.neteller")).click();
		
		
		if (component.isElementPresent(By.id("neteller"))) 
		 {
			driver.findElement(By.id("neteller")).clear();
			driver.findElement(By.id("neteller")).sendKeys("450224147121");
			driver.findElement(By.name("neteller_sid")).clear();
			driver.findElement(By.name("neteller_sid")).sendKeys("728615");
			driver.findElement(By.id("amount")).clear();
			driver.findElement(By.id("amount")).sendKeys("100");
			driver.findElement(By.cssSelector("form[name=\"regNTLRForm\"] > div.form-element > input[name=\"password\"]")).clear();
			driver.findElement(By.cssSelector("form[name=\"regNTLRForm\"] > div.form-element > input[name=\"password\"]")).sendKeys(user.getPassword());
			 //driver.findElement(By.linkText("Depositar")).click();
			driver.findElement(By.linkText("Añadir Neteller")).click();
		 }	
		else	
		{	driver.findElement(By.name("neteller_sid")).clear();
			driver.findElement(By.name("neteller_sid")).sendKeys("728615");
			driver.findElement(By.id("amount")).clear();
			driver.findElement(By.id("amount")).sendKeys("100");
		    driver.findElement(By.name("password")).clear();
		    driver.findElement(By.name("password")).sendKeys(user.getPassword());
			driver.findElement(By.linkText("Depositar")).click();
		}
		
		 if (component.isElementPresent(By.cssSelector("#loginErrorBox > p"))) 
		 { assertEquals("ERR_NTLR_NOCONTACT", driver.findElement(By.cssSelector("#loginErrorBox > p")).getText());
		 	 fail("ERR_NTLR_NOCONTACT");}
		 if (component.isElementPresent(By.cssSelector("#reg_cpm_info > p")))
		 {assertEquals("Inválido Neteller ID. Inválido Neteller SID. Inválido Confirmar Contraseña. Inválido Cantidad a depositar.", driver.findElement(By.cssSelector("#reg_cpm_info > p")).getText());
		 	 fail("Inválido Neteller ID. Inválido Neteller SID. Inválido Confirmar Contraseña. Inválido Cantidad a depositar.");}
		
		
		assertEquals("Depósito completado", driver.findElement(By.cssSelector("strong")).getText());
		assertEquals("Depósito completado, cantidad depositada: MXN 100.00", driver.findElement(By.xpath("//div[@id='content']/div/div/div[2]/center/p[2]")).getText());
	}
	
}

