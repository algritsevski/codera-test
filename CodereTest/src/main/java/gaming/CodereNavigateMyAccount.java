package gaming;

import codere.CodereComponent;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;


/*
  Codere ST1.8 - Ensuring that a logged in user can access the My Account page
  Codere ST1.9 - Navigating the My Account pages with the submenu
  (Registration Details, My Account, Deposit, Withdraw, Transactions)
  The login procedure must be done before
*/
public class CodereNavigateMyAccount {

    @ClassRule
    public static CodereLogin login = new CodereLogin();

    private CodereComponent component = CodereComponent.getInstance();
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = component.getDriver();
    }
    
    @Test
    public void testMyAccount() throws Exception {
        
    	component.navigate("portal");
        driver.findElement(By.id("myAccount")).click();
        driver.findElement(By.id("disconnect"));
        driver.findElement(By.id("myAccount")).click();

    }
    
    @Test
    public void testGoAccount() throws Exception {
        
    	component.navigate("portal?action=GoAccount");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Mi Cuenta[\\s\\S]*$"));
        
    }
    
    @Test
    public void testGoRegDetails() throws Exception {
       
    	component.navigate("portal?action=GoRegDetails");
    	assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Registro Completo\\. Para jugar con dinero real, por favor haga un depósito\\.[\\s\\S]*$"));
       
    }
    
    @Test
    public void testGoDeposit() throws Exception {
    	
        component.navigate("portal?action=GoDeposit");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Opciones de depósito[\\s\\S]*$"));
       
    }
    
    @Test
    public void testGoWithdraw() throws Exception {
        
    	component.navigate("portal?action=GoWithdraw");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Opciones de depósito[\\s\\S]*$"));
       
    }
    
    @Test
    public void testGoHist() throws Exception {
        
    	component.navigate("portal?action=GoHist");
    	 assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Historial de transacciones[\\s\\S]*$"));
    
    }

    @Test
    public void testGoUpdate() throws Exception {

        component.navigate("portal?action=GoUpdate");
        assertTrue(component.isElementPresent(By.id("updDetails")));
        
    }
    
    @Test
    public void testGoPwdUpdate() throws Exception {
        
    	component.navigate("portal?action=GoPwdUpdate");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Cambiar Contraseña[\\s\\S]*$"));
       
    }
    
    @Test
    public void testGoResponsible() throws Exception {
        
    	component.navigate("portal?action=GoResponsible");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Juego Responsable[\\s\\S]*$"));
      
    }
    
    @Test
    public void testGoCanvas() throws Exception {
        
    
        component.navigate("portal?action=GoCanvas&c=Payments");
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Información Sobre Pagos[\\s\\S]*$"));
       
    }
}
