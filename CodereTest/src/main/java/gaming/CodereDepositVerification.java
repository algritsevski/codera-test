package gaming;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;

public class CodereDepositVerification {

	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = component.getDriver();
	}

	@Test
	public void testCodereDepositVerivication() throws Exception {
		component.navigate("portal");

		driver.findElement(By.cssSelector("a.depositBtn")).click();

		if (component.isElementPresent(By.id("username"))) {
			driver.findElement(By.id("regSubmit")).click();
			// verification must be added
		}

		driver.findElement(By.cssSelector("a.depositBtn")).click();

		
		driver.findElement(By.cssSelector("a.paySafeCard")).click();

		if (component
				.isElementPresent(By
						.cssSelector("form[name=\"regPaySafeForm\"] > div.form-element > input[name=\"amount\"]")))
			;
		else
			fail("PaySafe Card");

		driver.findElement(By.cssSelector("a.dineroMail")).click();

		if (component.isElementPresent(By.name("provider")))
			;
		else
			fail("DineroMail");

		driver.findElement(By.cssSelector("a.neteller")).click();
		if (component.isElementPresent(By.id("neteller")))
			;
		else
			fail("Neteller Details");
	}
}
