package gaming;


import bo.DepositLimitsComponentBO;
import org.junit.Before;
import org.junit.ClassRule;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import codere.CodereComponent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CodereDepositLimitsVerification {

    String duration;

    @ClassRule
    public static CodereLogin login = new CodereLogin();

    private DepositLimitsComponentBO exludeComponent = DepositLimitsComponentBO.getInstance();
    private CodereComponent component = CodereComponent.getInstance();
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = component.getDriver();
    }


    @Test
    public void testDepositLimitsVerification() throws Exception {

            exludeComponent.depositlimits();
            component.navigate("portal?action=GoResponsible");


            driver.findElement(By.id("daily")).clear();
            driver.findElement(By.id("daily")).sendKeys("100");
            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("1000");
            driver.findElement(By.id("monthly")).clear();
            driver.findElement(By.id("monthly")).sendKeys("10000");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();



            assertEquals("100.00", driver.findElement(By.id("daily")).getAttribute("value"));
            assertEquals("1000.00", driver.findElement(By.id("weekly")).getAttribute("value"));
            assertEquals("10000.00", driver.findElement(By.id("monthly")).getAttribute("value"));

            exludeComponent.depositlimits();

            component.navigate("portal?action=GoResponsible");
            assertEquals("", driver.findElement(By.id("daily")).getAttribute("value"));
            assertEquals("", driver.findElement(By.id("weekly")).getAttribute("value"));
            assertEquals("", driver.findElement(By.id("monthly")).getAttribute("value"));

            driver.findElement(By.id("daily")).clear();
            driver.findElement(By.id("daily")).sendKeys("100");
            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("1000");
            driver.findElement(By.id("monthly")).clear();
            driver.findElement(By.id("monthly")).sendKeys("10000");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();


            driver.findElement(By.id("monthly")).clear();
            driver.findElement(By.id("monthly")).sendKeys("99000");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertTrue(component.isElementPresent(By.id("monthlyError")));

            component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("9000");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertTrue(component.isElementPresent(By.id("weeklyError")));

            component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("daily")).clear();
            driver.findElement(By.id("daily")).sendKeys("900");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();

            // must be uncomment in final version
            assertTrue(component.isElementPresent(By.id("dailyError")));

            exludeComponent.depositlimits();

            component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("daily")).clear();
            driver.findElement(By.id("daily")).sendKeys("100");
            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("1000");
            driver.findElement(By.id("monthly")).clear();
            driver.findElement(By.id("monthly")).sendKeys("10000");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();

            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("10001");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertEquals("Limite semanal es demasiado grande, debe ser menor que el mensual", driver.findElement(By.id("weeklyError")).getText());

            component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("daily")).clear();
            driver.findElement(By.id("daily")).sendKeys("1001");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertEquals("Limite diario es demasiado grande, debe ser menor que el semanal", driver.findElement(By.id("dailyError")).getText());

            component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("monthly")).clear();
            driver.findElement(By.id("monthly")).sendKeys("999");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertEquals("Limite semanal es demasiado grande, debe ser menor que el mensual", driver.findElement(By.id("weeklyError")).getText());

             component.navigate("portal?action=GoResponsible");
            driver.findElement(By.id("weekly")).clear();
            driver.findElement(By.id("weekly")).sendKeys("99");
            driver.findElement(By.cssSelector("input.buttonDefault")).click();
            assertEquals("Limite diario es demasiado grande, debe ser menor que el semanal", driver.findElement(By.id("dailyError")).getText());

           component.navigate("portal?action=GoResponsible");
           driver.findElement(By.id("monthly")).clear();
           driver.findElement(By.id("monthly")).sendKeys("99");
           driver.findElement(By.cssSelector("input.buttonDefault")).click();
           assertEquals("Limite semanal es demasiado grande, debe ser menor que el mensual", driver.findElement(By.id("weeklyError")).getText());
           assertEquals("Limite diario es demasiado grande, debe ser menor que el mensual", driver.findElement(By.id("dailyError")).getText());



           exludeComponent.depositlimits();

    }
}
