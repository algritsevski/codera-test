package gaming;

import codere.CodereComponent;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.*;

public class CodereNavigateMainTest {

    private CodereComponent component = CodereComponent.getInstance();
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = component.getDriver();
    }

    //Codere ST1.7 - Navigate using the Main menu on the website (Home, Casino, Bingo, Mobile, Promotions)
    @Test
    public void testNavigateMain() throws Exception {
        component.navigate("portal");
        if (component.isElementPresent(By.linkText("Casino"))) {
            driver.findElement(By.linkText("Casino")).click();

            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*SLOTS MÁS POPULARES[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("Todos los juegos")).click();

        }

        if (component.isElementPresent(By.linkText("Bingo"))) {
            driver.findElement(By.linkText("Bingo")).click();

            //component.waitElement(By.linkText("Terminos y condiciones"));
            try {
                assertTrue(component.isElementPresent(By.linkText("Terminos y condiciones")));
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("Terminos y condiciones")).click();
            try {
                assertEquals("Términos y Condiciones de Bingo", driver.findElement(By.cssSelector("div.content > h1")).getText());
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("Reglas")).click();
            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Reglas del BINGO e instrucciones para comenzar[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("Glosario")).click();
            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Glosario de Bingo[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("PMF")).click();
            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Preguntas frecuentes sobre el bingo[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }

            driver.findElement(By.linkText("Reglas del chat")).click();
            try {
                assertEquals("Términos y Condiciones del Chat", driver.findElement(By.cssSelector("div.content > h1")).getText());
            } catch (Error e) {
                fail(e.toString());
            }
            driver.findElement(By.linkText("Home")).click();
            try {
                assertTrue(component.isElementPresent(By.xpath("(//img[@alt='mega_jackpots_banner'])[5]")));
            } catch (Error e) {
                fail(e.toString());
            }
        }

        if (component.isElementPresent(By.linkText("Movil"))) {
            driver.findElement(By.linkText("Movil")).click();

            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*MOBILE CASINO[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }
//			component.navigate("portal?action=GoMobile#tab3");
//			//driver.findElement(By.linkText("Iphone Casino")).click();
//			try {
//				assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Lorem ipsum dolor sit amet, consectetur adipiscing elit\\. Ut semper, mi fermentum ornare vulputate, neque libero suscipit ante, vitae venenatis metus risus ut quam\\. Nulla non nulla felis, condimentum sodales orci\\. Etiam sed quam a dolor lacinia fringilla\\. Donec faucibus, tortor at mattis dictum, sem purus elementum ante, in venenatis justo nulla quis mauris\\. Mauris pharetra porta tincidunt\\. Proin elit nulla, lacinia quis posuere sit amet, lacinia in nibh\\. Mauris luctus lacinia odio ut malesuada\\. Duis blandit vestibulum egestas\\.[\\s\\S]*$"));
//			} catch (Error e) {
//				fail(e.toString());
//			}
//
//			driver.findElement(By.linkText("Casino Games")).click();
//			try {
//				assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Lorem ipsum dolor sit amet, consectetur adipiscing elit\\. Ut semper, mi fermentum ornare vulputate, neque libero suscipit ante, vitae venenatis metus risus ut quam\\. Nulla non nulla felis, condimentum sodales orci\\. Etiam sed quam a dolor lacinia fringilla\\. Donec faucibus, tortor at mattis dictum, sem purus elementum ante, in venenatis justo nulla quis mauris\\. Mauris pharetra porta tincidunt\\. Proin elit nulla, lacinia quis posuere sit amet, lacinia in nibh\\. Mauris luctus lacinia odio ut malesuada\\. Duis blandit vestibulum egestas\\. Vestibulum malesuada, erat nec auctor tempus, massa diam venenatis eros, nec pellentesque sem ipsum vitae sapien\\. Suspendisse consectetur, quam eget semper convallis, metus tortor mollis urna, vitae blandit nibh erat eget mi\\. Vivamus luctus sem eu sapien dictum non auctor tellus fringilla\\. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas\\. Fusce non nibh tortor\\. Ut gravida urna purus\\. Donec id metus eu nibh consequat faucibus ac sed nibh\\. Suspendisse dignissim feugiat semper\\. Etiam nec quam metus\\. Duis malesuada, diam quis ornare rutrum, nulla risus eleifend massa, eu adipiscing urna quam eu diam\\. Aliquam erat volutpat\\. Nulla lacinia pretium nulla, sed tincidunt lorem ultricies et\\.Lorem ipsum dolor sit amet, consectetur adipiscing elit\\. Ut semper, mi fermentum ornare vulputate, neque libero suscipit ante, vitae venenatis metus risus ut quam\\. Nulla non nulla felis, condimentum sodales orci\\. Etiam sed quam a dolor lacinia fringilla\\. Donec faucibus, tortor at mattis dictum, sem purus elementum ante, in venenatis justo nulla quis mauris\\. Mauris pharetra porta tincidunt\\. Proin elit nulla, lacinia quis posuere sit amet, lacinia in nibh\\. Mauris luctus lacinia odio ut malesuada\\. Duis blandit vestibulum egestas\\. Vestibulum malesuada, erat nec auctor tempus, massa diam venenatis eros, nec pellentesque sem ipsum vitae sapien\\. Suspendisse consectetur, quam eget semper convallis, metus tortor mollis urna, vitae blandit nibh erat eget mi\\. Vivamus luctus sem eu sapien dictum non auctor tellus fringilla\\. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas\\. Fusce non nibh tortor\\. Ut gravida urna purus\\. Donec id metus eu nibh consequat faucibus ac sed nibh\\. Suspendisse dignissim feugiat semper\\. Etiam nec quam metus\\. Duis malesuada, diam quis ornare rutrum, nulla risus eleifend massa, eu adipiscing urna quam eu diam\\. Aliquam erat volutpat\\. Nulla lacinia pretium nulla, sed tincidunt lorem ultricies et\\.[\\s\\S]*$"));
//			} catch (Error e) {
//				fail(e.toString());
//			}
//			driver.findElement(By.linkText("Android")).click();
//			try {
//				assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Lorem ipsum dolor sit amet, consectetur adipiscing elit\\. Ut semper, mi fermentum ornare vulputate, neque libero suscipit ante, vitae venenatis metus risus ut quam\\. Nulla non nulla felis, condimentum sodales orci\\. Etiam sed quam a dolor lacinia fringilla\\. Donec faucibus, tortor at mattis dictum, sem purus elementum ante, in venenatis justo nulla quis mauris\\. Mauris pharetra porta tincidunt\\. Proin elit nulla, lacinia quis posuere sit amet, lacinia in nibh\\. Mauris luctus lacinia odio ut malesuada\\. Duis blandit vestibulum egestas\\. Vestibulum malesuada, erat nec auctor tempus, massa diam venenatis eros, nec pellentesque sem ipsum vitae sapien\\. Suspendisse consectetur, quam eget semper convallis, metus tortor mollis urna, vitae blandit nibh erat eget mi\\. Vivamus luctus sem eu sapien dictum non auctor tellus fringilla\\. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas\\. Fusce non nibh tortor\\. Ut gravida urna purus\\. Donec id metus eu nibh consequat faucibus ac sed nibh\\. Suspendisse dignissim feugiat semper\\. Etiam nec quam metus\\. Duis malesuada, diam quis ornare rutrum, nulla risus eleifend massa, eu adipiscing urna quam eu diam\\. Aliquam erat volutpat\\. Nulla lacinia pretium nulla, sed tincidunt lorem ultricies et\\.[\\s\\S]*$"));
//			} catch (Error e) {
//				fail(e.toString());
//			}
//
//			driver.findElement(By.linkText("Promotions")).click();
//
//			try {
//				assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Lorem ipsum dolor sit amet, consectetur adipiscing elit\\. Ut semper, mi fermentum ornare vulputate, neque libero suscipit ante, vitae venenatis metus risus ut quam\\. Nulla non nulla felis, condimentum sodales orci\\. Etiam sed quam a dolor lacinia fringilla\\. Donec faucibus, tortor at mattis dictum, sem purus elementum ante, in venenatis justo nulla quis mauris\\. Mauris pharetra porta tincidunt\\. Proin elit nulla, lacinia quis posuere sit amet, lacinia in nibh\\. Mauris luctus lacinia odio ut malesuada\\. Duis blandit vestibulum egestas\\. Vestibulum malesuada, erat nec auctor tempus, massa diam venenatis eros, nec pellentesque sem ipsum vitae sapien\\. Suspendisse consectetur, quam eget semper convallis, metus tortor mollis urna, vitae blandit nibh erat eget mi\\. Vivamus luctus sem eu sapien dictum non auctor tellus[\\s\\S]*$"));
//			} catch (Error e) {
//				fail(e.toString());
//			}
//
//			driver.findElement(By.linkText("Welcome")).click();
//			try {
//				assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*$"));
//			} catch (Error e) {
//				fail(e.toString());
//			}
//
        }

        if (component.isElementPresent(By.linkText("Promociones"))) {
            driver.findElement(By.linkText("Promociones")).click();

            try {
                assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Promotions Content[\\s\\S]*$"));
            } catch (Error e) {
                fail(e.toString());
            }
        }

        if (component.isElementPresent(By.linkText("Inicio"))) {

            driver.findElement(By.linkText("Inicio")).click();

            try {
                assertEquals("Bingo", driver.findElement(By.cssSelector("h2")).getText());
            } catch (Error e) {
                fail(e.toString());
            }
        }
    }
}
