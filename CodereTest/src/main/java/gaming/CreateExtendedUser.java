package gaming;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import codere.CodereComponent;
import domain.User;

public class CreateExtendedUser implements UserCreator {

    private CodereComponent component = CodereComponent.getInstance();

    public void add(User user) {
        WebDriver driver = component.getDriver();

        component.navigate("portal?action=GoRegister");

        String userId = user.getName();

        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(userId);
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys(user.getPassword());
        driver.findElement(By.id("vfy_password")).clear();
        driver.findElement(By.id("vfy_password")).sendKeys(user.getPassword());
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(userId + "@igt.com");
        driver.findElement(By.id("vfy_email")).clear();
        driver.findElement(By.id("vfy_email")).sendKeys(userId + "@igt.com");

        new Select(driver.findElement(By.id("day"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("month"))).selectByVisibleText("1");
        new Select(driver.findElement(By.id("year"))).selectByVisibleText("1976");
        driver.findElement(By.id("agree")).click();
        driver.findElement(By.id("regContinue")).click();
        
        
        driver.findElement(By.id("response_1")).clear();
        driver.findElement(By.id("response_1")).sendKeys(userId);
        driver.findElement(By.id("fname")).clear();
        driver.findElement(By.id("fname")).sendKeys(userId + "First");
        driver.findElement(By.id("lname")).clear();
        driver.findElement(By.id("lname")).sendKeys(userId + "Last");
        driver.findElement(By.id("ssurname")).clear();
        driver.findElement(By.id("ssurname")).sendKeys(userId + "Surname");
        driver.findElement(By.xpath("(//input[@name='gender'])[2]")).click();
        driver.findElement(By.name("gender")).click();
        driver.findElement(By.id("addr_street_1")).clear();
        driver.findElement(By.id("addr_street_1")).sendKeys(userId + "Address");

        new Select(driver.findElement(By.id("addr_state_id"))).selectByVisibleText("Jalisco");

        driver.findElement(By.id("addr_city")).clear();
        driver.findElement(By.id("addr_city")).sendKeys(userId + "City");
        driver.findElement(By.id("telephone")).clear();
        driver.findElement(By.id("telephone")).sendKeys("1000000");
       
        driver.findElement(By.id("regFull")).click();
    }
}
