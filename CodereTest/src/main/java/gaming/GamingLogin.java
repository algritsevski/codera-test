package gaming;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;

import codere.CodereComponent;

@Ignore
public class GamingLogin {

	@ClassRule
	public static CodereLogin login = new CodereLogin();

	private CodereComponent component = CodereComponent.getInstance();

	@Before
	public void before() {
		login.logout();
	}

	@After
	public void after() throws Exception {
		login.login();
	}

	@Test
	public void login() {
		assertTrue(component.isElementPresent(By.id("myAccount")));
		assertTrue(component.isElementPresent(By.cssSelector("a.depositBtn")));
	}
}
